# Introdução

O Athena é um serviço interativo de execução de queries, que torna fácil analizar dados no S3 usando SQL padrão sem precisar de mover os dados para um banco relacional. O Athena é servelles, logo não precisa se preocupar com infraestrutura, sendo que o athena automaticamente escala seu cluster de processamento para lidar com o dataset de acordo com a quantidade de dados que ele possuir, e você paga apenas pelas queries que executa.

Quando você registra uma tabela no athena com os dados do S3, ele armazena o mapeamento dos dados em um data catalog. O Athena utiliza o AWS Glue Data Catalog para isso. Sendo assim quando quando criar um banco ou uma tabela no Athena ela vai aparecer no Glue, e esses metadados poderão ser acessados por outros serviços, como por exemplo o Redshift Spectrum.

O Athena suporta um sistema de partições, sendo que você pode criar até 10 milhões de partições para leitura de arquivos parquet. As partições vão auxiliar na velocidade das queries, guiando o athena da melhor forma extrair os dados, assim otimizando as queries. Se olharmos para o mundo de bancos relacionais, seria o equivalente a criar index (equivalente, mas bem diferente).

Depois de entender os exemplos abaixo, seria bom dar uma olhada nesse artigo [aqui](https://aws.amazon.com/pt/blogs/big-data/top-10-performance-tuning-tips-for-amazon-athena/) sobre performance do athena e como melhorá-la.

# Exemplo 01

- Vou copiar um arquivo do bucket publico da amazon de reviews de clientes, nele temos muitos arquivos, mas eu vou pegar apenas dois arquivos, para não demorar muito e também não passar do free tier

```sh
aws s3 cp --recursive s3://amazon-reviews-pds/tsv/ s3://<bucket>/athena/input_data/amazon-reviews-pds/tsv/ --exclude "*" --include "amazon_reviews_us_Software_v1_00.tsv.gz"
aws s3 cp --recursive s3://amazon-reviews-pds/tsv/ s3://<bucket>/athena/input_data/amazon-reviews-pds/tsv/ --exclude "*" --include "amazon_reviews_us_Books_v1_00.tsv.gz"
```

Agora para rodar as queries, basta entrar no console do athena. E a primeira coisa que deve fazer, é configurar um local para onde ele irá jogar os resultados das queries, para isso basta ir em `Settings` e lá apontar algum local no S3 (query result location), no meu caso eu especifiquei `s3://<bucket>/athena/athena_results/`

Agora na aba de Query Editor você verá que tem um local para digitar queries

- Crie um banco de dados
```sql
create database dsoaws
```

Se você for no Glue, na sessão de databases você verá esse banco por lá

- Criar a tabela
```sql
CREATE EXTERNAL TABLE IF NOT EXISTS dsoaws.amazon_reviews_tsv(
         marketplace string,
         customer_id string,
         review_id string,
         product_id string,
         product_parent string,
         product_title string,
         product_category string,
         star_rating int,
         helpful_votes int,
         total_votes int,
         vine string,
         verified_purchase string,
         review_headline string,
         review_body string,
         review_date string 
) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' 
  LINES TERMINATED BY '\n' 
  LOCATION 's3://<bucket>/athena/input_data/amazon-reviews-pds/tsv/' 
  TBLPROPERTIES (
      'compressionType'='gzip', 
      'skip.header.line.count'='1'
  )
```

No Glue, na sessão de tables, você verá essa tabela.

- Faça uma querie qualquer
```sql
SELECT * FROM dsoaws.amazon_reviews_tsv 
WHERE product_category='Software' LIMIT 10
```

Os resultados foram:
```ruby
Run time: 1 min 14.398 sec 
Data scanned: 2.55 GB
```

- Convertendo de TSV para Parquet e adicionando uma partição por categoria de produtos
```sql
CREATE TABLE IF NOT EXISTS dsoaws.amazon_reviews_parquet
WITH (
    format = 'PARQUET', 
    external_location = 's3://<bucket>/athena/input_data/amazon-reviews-pds/parquet', 
    partitioned_by = ARRAY['product_category']
) AS
SELECT 
    marketplace,
    customer_id,
    review_id,
    product_id,
    product_parent,
    product_title,
    star_rating,
    helpful_votes,
    total_votes,
    vine,
    verified_purchase,
    review_headline,
    review_body,
    CAST(YEAR(DATE(review_date)) AS INTEGER) AS year,
    DATE(review_date) AS review_date,
    product_category
FROM dsoaws.amazon_reviews_tsv
```

- Agora vamos carregar as partições
```sql
MSCK REPAIR TABLE amazon_reviews_parquet
```

- Agora rode a query novamente e veja a diferença para o TSV
```sql
SELECT * FROM dsoaws.amazon_reviews_parquet 
WHERE product_category='Software' LIMIT 10
```

```ruby
Run time: 0.786 sec
Data scanned: 11.10 MB
```

# Exemplo 02
Vou criar algo bem semelhante ao primeiro, porém ao em vez de criar a tabela manualmente, eu vou usar um crawler no Glue

Então vou deixar os mesmos dados de antes lá no S3 (vou tabalhar com os arquivos parquet) e no console do Athena, vou em Data Sources e em Connect data Source

<img src="./images/fig_01.png"/>

Agora escolha a opção `S3 - AWS Glue Data Catalog` e mantenha o restante com o default e então click em `Create in AWS Glue`

<img src="./images/fig_02.png"/>

Agora você será redirecionado ao Glue, e lá você criará o seu crawler

<img src="./images/fig_03.png"/>

-

<img src="./images/fig_04.png"/>

-

<img src="./images/fig_05.png"/>

-

<img src="./images/fig_06.png"/>

-

<img src="./images/fig_07.png"/>

-

<img src="./images/fig_08.png"/>

-

<img src="./images/fig_09.png"/>

-

<img src="./images/fig_10.png"/>

Assim que acabar de criar o crawler apenas mande-o executar. Para isso basta ir no console do Glue e em crawler, selecione o seu e de um Run Crawler

<img src="./images/fig_11.png"/>

Agora você pode ir no Athena, recarregar a página e verá que o banco default agora tem uma tabela. E você poderá fazer nela as suas queries 


> OBS.: Você paga pelo crawler, então é melhor deletar tudo depois

# Exemplo 03
Agora vou executar a query utilizando a SDK

```py
import json
import boto3

client = boto3.client('athena')

bucket = '<bucket>'

query = """
    SELECT * FROM dsoaws.amazon_reviews_parquet 
    WHERE product_category='Software' LIMIT 10
"""

response = client.start_query_execution(
    QueryString=query,
    QueryExecutionContext={
        'Database': 'dsoaws'
    },
    ResultConfiguration={
        'OutputLocation': f's3://{bucket}/athena/athena_results/'
    }
)

query_id = response['QueryExecutionId']

response = client.get_query_results(
    QueryExecutionId=query_id
)

with open('response.json', 'w') as f:
    json.dump(response, f, indent=4)
```
